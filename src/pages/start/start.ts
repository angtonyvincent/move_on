import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-start',
  templateUrl: 'start.html'
})
export class StartPage {
  name : any;
  activity : any;
  aim : "Time";
  label = "Minute";
  distanceSelected = false;
  value1 : any;
  value2 : any;

  constructor(public navCtrl: NavController) {

  }

  onAimChange(value)
  {
    if (value == "Distance")
    {
      this.distanceSelected = true;
      this.label = "Km";
    }
    else
    {
      this.distanceSelected = false;
      this.label = "Minute";
    }
  }

  save()
  {
    console.log("name " + this.name
      + " activity " + this.activity
      + " aim " + this.aim
      + " value " + this.value1 + " & " + this.value2);
  }

}
